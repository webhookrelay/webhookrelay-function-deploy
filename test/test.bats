#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/webhookrelay-function-deploy:latest"}

  echo "Building image..."
  run docker build -t ${DOCKER_IMAGE} .

  run wget -O /bin/relay https://storage.googleapis.com/webhookrelay/downloads/relay-linux-amd64
  run chmod a+x /bin/relay

  FUNCTION_NAME="whr-functions-deploy-test-app-${BITBUCKET_BUILD_NUMBER}"

  run relay function create --driver lua --name ${FUNCTION_NAME} ${BATS_TEST_DIRNAME}/function.lua    
}

teardown() {
    echo "Teardown happens after each test."
    FUNCTION_NAME="whr-functions-deploy-test-app-${BITBUCKET_BUILD_NUMBER}"
    relay function rm ${FUNCTION_NAME}
}

@test "Deploy Function" {

  run docker run \
        -e FUNCTION_NAME=${FUNCTION_NAME} \
        -e RELAY_KEY=${RELAY_KEY} \
        -e RELAY_SECRET=${RELAY_SECRET} \
        -e FUNCTION_FILE=${BATS_TEST_DIRNAME}/function.lua \
        -v $(pwd):$(pwd) \
        -v $(pwd)/tmp:/tmp \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

  echo ${output}
  [ "$status" -eq 0 ]

  run relay function invoke ${FUNCTION_NAME} --method PUT --body "hi there"
  [ "$status" -eq 0 ]
  [ "${lines[1]}" = "Response modified: true" ]
  [ "${lines[4]}" = "RESPONSE" ]
  
}