image:
	docker build -t webhookrelay/bitbucket-functions-deploy:dev -f Dockerfile .

push: image
	docker push webhookrelay/bitbucket-functions-deploy:dev

.PHONY:test
test:
	bats test/test.bats