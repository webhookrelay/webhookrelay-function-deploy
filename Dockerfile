FROM webhookrelay/relay:1.24.2 as relay-env

FROM alpine:3.9

RUN apk --no-cache add \
    bash=4.4.19-r1
RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh
# RUN wget -O /bin/relay https://storage.googleapis.com/webhookrelay/downloads/relay-linux-amd64
# Get relay binary
COPY --from=relay-env /bin/relay /bin/relay

COPY pipe /
RUN chmod a+x /*.sh
# RUN chmod a+x /bin/relay
COPY LICENSE.txt README.md pipe.yml /

# Disabling relay CLI updates
ENV RELAY_UPDATES_DISABLE=true

# ENTRYPOINT ["/pipe.sh"]
CMD ["/pipe.sh"]