# Bitbucket Pipelines Pipe: Webhook Relay Function Deploy

A pipe that deploys Webhook Relay functions. Functions can be attached to bucket public endpoints (Inputs) or destinations (Outputs) to modify, transform and filter webhooks or API requests. 

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: webhookrelay/webhookrelay-function-deploy:0.2.5
    variables:
      FUNCTION_NAME: '<string>'
      FUNCTION_FILE: '<string>'
      RELAY_KEY: $RELAY_KEY
      RELAY_SECRET: $RELAY_SECRET
```

## Variables

| Variable               | Usage                                                                   |
| ---------------------- | ----------------------------------------------------------------------- |
| FUNCTION_NAME (*)      | Function name in Webhook Relay (https://my.webhookrelay.com/functions)  |
| FUNCTION_FILE (*)      | Path of the function file that should be deployed                       |
| RELAY_KEY (*)          | Access token key (generate one at https://my.webhookrelay.com/tokens)   |
| RELAY_SECRET (*)       | Access token secret (generated together with the key)                   |

_(*) = required variable._

## Prerequisites

An already created function (https://my.webhookrelay.com/functions). Current version of the pipe is not creating new functions. 

## Examples

Basic example:

```yaml
script:
  - pipe: webhookrelay/webhookrelay-function-deploy:0.2.5
    variables:
      FUNCTION_NAME: 'dockerhub_to_slack'
      FUNCTION_FILE: 'dockerhub_to_slack.lua'
      RELAY_KEY: $RELAY_KEY
      RELAY_SECRET: $RELAY_SECRET
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, please contact us:

**Option 1** - Webhook Relay portal - Goto: https://my.webhookrelay.com and click on the chat symbol in the bottom right corner.
**Option 2** - Email info@webhookrelay.com.
**Option 3** - Submit a form via https://my.webhookrelay.com/contact. 

If you’re reporting an issue, please include:
- the version of the pipe
- relevant logs and error messages
- steps to reproduce
