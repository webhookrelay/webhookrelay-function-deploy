#!/usr/bin/env bash
#
# Deploy Webhook Relay functions, https://webhookrelay.com/v1/guide/functions
#
# Required globals:
#  FUNCTION_NAME
#  FUNCTION_FILE
#  RELAY_KEY
#  RELAY_SECRET

source "$(dirname "$0")/common.sh"

# mandatory variables
FUNCTION_NAME=${FUNCTION_NAME:?'FUNCTION_NAME variable missing.'}
FUNCTION_FILE=${FUNCTION_FILE:?'FUNCTION_FILE variable missing.'}
RELAY_KEY=${RELAY_KEY:?'RELAY_KEY variable missing.'}
RELAY_SECRET=${RELAY_SECRET:?'RELAY_SECRET variable missing.'}

info "Uploading function..."

run relay function update -s ${FUNCTION_FILE} ${FUNCTION_NAME}