# Release instructions

Release is performed using Bitbucket pipelines:

```
git tag <new version>         
git push && git push --tags
```