# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.3

- patch: update pipe.yml

## 0.2.4

- patch: updated readme

## 0.2.5

- patch: ensuring that relay CLI will not try to update itself when run in the container